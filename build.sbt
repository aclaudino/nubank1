name := "Nubank"

version := "1.0"

fork in run := true
connectInput in run := true

baseDirectory in run := file("resource/")

mainClass in (Compile, run) := Some("br.com.claudino.nubank.proj1.Nubank")

scalaVersion := "2.11.8"
libraryDependencies ++= Seq(
   "org.scala-lang" % "scala-library" % scalaVersion.value,
   "org.scala-lang" % "scala-compiler" % scalaVersion.value % "scala-tool"   
)

lazy val http4sVersion = "0.13.2"
libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion withSources(),
  "org.http4s" %% "http4s-blaze-server" % http4sVersion withSources(),
  "org.http4s" %% "http4s-blaze-client" % http4sVersion withSources()
)

