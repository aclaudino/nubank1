# Projeto Nubank1 por André Claudino
*por [André Claudino][mail]*
## Uso
O projeto foi desenvolvido em linguagem [*Scala*][scala] e organizado via [*SBT*][sbt] e faz uso da biblioteca [*http4s*][hht4s]. Para compilar, basta acessar o diretório onde se encontra o arquivo **Build.sbt** (raiz) e digitar o comando:

    > sbt compile

para executar, o comando

    > sbt run

Será iniciado um servidor em <http://localhost:8080/convites>. O programa se encerrará quando uma tecla seja pressionada no console.

Esta API responde a duas "demandas":

Uma é a demanda de consumo, a partir do verbo **GET** no endpoint **/convites**, que lista os cadastrados

    GET /convites
    Listará um json com os convites cadastrados até então

Outra é ao VERBO **PUT** no endpoint /convite com dois parâmetros, que inclui um novo convite

    PUT /convites/:item1/:item2
    Incluirá um convite do indivídio de **id** :item1 para o individio de **id** :item2
    
Ou seja:

    PUT /convites/100/101
    Significa que o indivíduo de id 100 está convidando o indivíduo de id 101

## Descrição

A ideia para resolver o problema foi utilizar um modelo de redes complexas, que baseia no apontamento de objetos para uma lista de filhos, partindo do de um cabeçalho com todos os elementos listados. Dessa forma, a ideia de algoritmos de busca em grafos se torna desnecessária, já que todo elemento presente se encontra nessa lista de cabeçalho. No programa esta lista foi chamada de **rede**.

### Adição de um par: 
* Quando um novo par **item**->**item2** é apresentado, o algoritmo procura **item1** em **rede**, se o encontrar, pega a referência para este objeto segue par aa busca por **item2**
* Se **item2** for encontrado na rede, significa que ele não pode ser convidado, então a adição é encerrada sem que o convite seja feito.
* Caso contrário, se **item2** não está na rede, significa que ele pode ser convidado. **item2** é adicionado a rede e em seguida adicionado a lista de filhos de **item1** e **item1** é referenciado como pai de **item2**

### Contagem de pontos
* Quando um filho é adicionado, quem o convidou é deinido como seu pai. Portanto, é chamado o método **somaPonto()** no filho. Este método verifica de forma recursiva se o pai do elemento em questão existe, caso exista, este tem a pontuação correspondente ao convite em profundidade computado. Comomo já foi dito, o método é recursivo, e funciona somando a pontuação atual de um elemento, o valor correspondente a $$1/2^n$$, onde $$n$$ é o nível de distância (em número de elementos) entre o elemento cuja pontuação é calculada e o que tem seu filho adicionado.

   [scala]: <http://www.scala-lang.org/>
   [sbt]: <http://www.scala-sbt.org/index.html>
   [http4s]: <http://http4s.org/>
   [mail]: <mailto:claudino@d2x.com.br>
   