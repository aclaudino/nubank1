
package br.com.claudino.nubank.proj1

import scala.math.pow

class Item (var id : String, var pai : Item = null, var filhos : List[Item] = List[Item](), var pontos : Double = 0.0){
   
  /* Adiciona um filho, ou seja, um convidado */
  def adicionarFilho(item : Item){
    //Verifica a presença do filho
    if(isFilho(item)){
      return
    }
    
    /// Se ainda não é filho, adiciona
    item.pai = this
    this.filhos = item :: this.filhos
    /// Adiciona a pontuação aos pais superiores
    somaPonto()
  }
  
  /* Verifica se um dado ítem é um convidado */
  def isFilho(item : Item) : Boolean = (this.filhos != null && this.filhos.indexOf(item) >= 0)
  
  /*Adiciona os pontos correspondentes aos ítens superiores. É disparado ao adicionar um filho*/
  def somaPonto(nivel : Int = 0){
    if(pai != null){
      pai.pontos = pai.pontos + pow(0.5, nivel)
      pai.somaPonto(nivel+1)
    }
  }
  
  /* Método acessório para comparaçao, verifica se o
   * objeto a ser comparado é do tipo Item
   */
  def canEqual(a : Any) = a.isInstanceOf[Item]
  
  /* Método que compara a igualdade de dois ítens:
   * dois ítens são considerados iguals se tem o mesmo
   * id, independentemente dos seus filhos serem ou não
   * os mesmos. Essa igualdade serve para comparar presença
   * ou não de um elemento em alguma lista.
   */
  override def equals(outro: Any) : Boolean = {
    outro match {
      case outro : Item =>
        outro.canEqual(this) && this.id == outro.id
      case _ => false
    }
  }
  
  /* Sobrecarga do operador de igualdade para comparação
   * direta Item a Item.
   */
  def ==(item : Item) : Boolean = (id == item.id)
  
  /* Gera uma string no formato Json com o objeto em questão */
  override def toString() : String = {
    var s : String = "id : " + id.toString();
    var p : String = "pontos : " + pontos.toString()
    var f : String = "filhos : [" + filhos.mkString(", ") + "]"
    return "{"+ s +", " + p + ", " + f + "}"
  }
  
}
