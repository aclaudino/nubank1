package br.com.claudino.nubank.proj1

import scala.io._
import util.control.Breaks._
import org.http4s._
import org.http4s.dsl._
import org.http4s.server.blaze._

object Nubank extends App {

  /// Cabeçalho da rede complexa onde estarão todos os elementos
  var rede: List[Item] = _
  /// O Descritor é um resumo do cabeçalho da rede, que conterá apenas os elementos de primeiro nível
  var descritor: List[Item] = _
  /// Variável que contém a rede em formato json, armazenada para cache entre diferentes consultas
  var json : String =_
  
  /* Método principal. Neste método é originalmente
   * carregado o arquivo de entrada (inputs.txt) e
   * e após isto, é iniciado o servidor http
   * com o endpoint de consulta e o de alteração
   */
  override def main(args: Array[String]) {

    rede = List[Item]()
    descritor = List[Item]()
    
    adicionarDoArquivo("input.txt")
    
    formatJson()
    
    serviceStart()
    
    println("fim")

  }
  
  /* Este método varre o arquivo e adiciona
   * os pares encontrados
   */
  def adicionarDoArquivo(nomeArquivo: String) {

    var i: Int = 0;
    for (linha <- Source.fromFile(nomeArquivo).getLines) {
      breakable {

        var s: Array[String] = linha.split(" ")

        var item1: Item = new Item(s(0))
        var item2: Item = new Item(s(1))

        if (rede.indexOf(item1) < 0) {
          /// Elemento não está na rede
          rede = item1 :: rede
          /// Se não está na rede, entra no descritor
          descritor = item1 :: descritor
        } else {
          item1 = rede(rede.indexOf(item1))
        }

        if (rede.indexOf(item2) < 0) {
          rede = item2 :: rede
          item1.adicionarFilho(item2)
        } else {
          break
        }
      }
    }

  }

  /*
   * Este método faz a adição de apenas um par, será
   * utilizado na api
   */
  def adicionarPar(id1: String, id2: String) : String = {
    var item1: Item = new Item(id1)
    var item2: Item = new Item(id2)

    if (rede.indexOf(item1) < 0) {
      /// Elemento não está na rede
      rede = item1 :: rede
      /// Se não está na rede, entra no descritor
      descritor = item1 :: descritor
    } else {
      item1 = rede(rede.indexOf(item1))
    }

    if (rede.indexOf(item2) < 0) {
      rede = item2 :: rede
      item1.adicionarFilho(item2)
      formatJson()
      return "Par Adicionado"
    } else {
      return s"$item2 já convidado"
    }

  }
  
  /*Método usado par gerar um json a partir
   * do descritor
   */
  def formatJson(){    
    json ="["
      for (r <- descritor) {
        json+= r.toString() + ", "
      }
    json+="]"
  }
  
  /*Método que inicializa o servidor e aguarda
   * sua finalização (ao pressionar uma tecla)
   */
  def serviceStart() = {
    
    val service = HttpService {
      case GET -> Root / "convites" =>
        Ok(json)
      case PUT -> Root/ "convites"/ item1 / item2 =>
        Ok(adicionarPar(item1, item2))
    }
    
    val builder = BlazeBuilder.mountService(service)
    val server = builder.run
    
    println("Pressione uma tecla para terminar")
    System.in.read()
    server.shutdownNow()
    
  }
  
}